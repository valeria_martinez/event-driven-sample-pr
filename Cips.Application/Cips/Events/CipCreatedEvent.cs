using Cips.Application.Events;

namespace Cips.Application.Cips.Events
{
    public class CipCreatedEvent: EventBase
    {
        public CipCreatedEvent(object eventData) : base(eventData)
        {
        }

        public override string GetEventId()
        {
            return "cips.CipCreated.1";
        }
        
    }
}